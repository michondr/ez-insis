limit_req_zone $binary_remote_addr zone=baseLimit:10m rate=1r/s;
limit_req_status 429;

server {
    server_name {{ nginx_host }}.michondr.cz;
    root {{ nginx_app_root }};

    # http://nginx.org/en/docs/http/ngx_http_limit_req_module.html
    limit_req zone=baseLimit burst=5 nodelay;

    location ~ ^/$ {
        # todo redirect to apiari
        return 301 https://ezinsis.docs.apiary.io/#;
    }

    location = /favicon.ico {
        access_log off;
        return 204;
    }

    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_param SYMFONY_ENV prod;
        fastcgi_param APP_ENV prod;
        fastcgi_param APP_SECRET 9ce53ad2002fe482db65e473465c7762;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;

        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
