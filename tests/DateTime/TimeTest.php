<?php

namespace App\DateTime;

use PHPUnit\Framework\TestCase;

class TimeTest extends TestCase
{
    //todo test
    public function testFromHI1()
    {
        $str = '12:30';
        $time = Time::fromHI($str);

        self::assertSame(12, $time->getHour());
        self::assertSame(30, $time->getMinute());
        self::assertSame(0, $time->getSecond());
    }

    public function testFromHI2()
    {
        $str = '15:26';
        $time = Time::fromHI($str);

        self::assertSame(15, $time->getHour());
        self::assertSame(26, $time->getMinute());
        self::assertSame(0, $time->getSecond());
    }

    public function testFromHI3()
    {
        $str = '07:18';
        $time = Time::fromHI($str);

        self::assertSame(7, $time->getHour());
        self::assertSame(18, $time->getMinute());
        self::assertSame(0, $time->getSecond());
    }

    public function testToHI1()
    {
        $str = '07:18';
        $time = Time::fromHI($str);
        self::assertSame($str, $time->toHI());

        $str = '23:50';
        $time = Time::fromHI($str);
        self::assertSame($str, $time->toHI());

        $str = '2:6';
        $time = Time::fromHI($str);
        self::assertSame($str, $time->toHI());
    }

    public function testToTimestamp()
    {

    }
}
