<?php

namespace App\Tests\TimetableBuilder\Timetable;

use App\TimetableBuilder\Timetable\Timetable;
use PHPUnit\Framework\TestCase;

class TimetableTest extends TestCase
{
    /** @var $timetable Timetable */
    private $timetable;

    public function testConstruct()
    {
        $expectedDays = [
            'mon' => [],
            'tue' => [],
            'web' => [],
            'thu' => [],
            'fri' => [],
            'sat' => [],
            'sun' => [],
        ];

        self::assertEquals($expectedDays, $this->timetable->getDays());
    }

    protected function setUp(): void
    {
        $this->timetable = new Timetable();
    }

}
