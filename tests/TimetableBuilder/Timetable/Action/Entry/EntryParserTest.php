<?php

namespace App\Tests\TimetableBuilder\Timetable\Action\Entry;

use App\TimetableBuilder\Timetable\Action\Entry\Entry;
use App\TimetableBuilder\Timetable\Action\Entry\EntryParser;
use PHPUnit\Framework\TestCase;

class EntryParserTest extends TestCase
{
    public function testParse1()
    {
        $dataJson = '{"entry":"seminar"}';
        $dataArray = json_decode($dataJson, true);

        $room = EntryParser::parse($dataArray['entry']);

        $expected = new Entry('seminar');

        self::assertEquals($expected, $room);
    }

    public function testParse2()
    {
        $dataJson = '{"entry":"lecture"}';
        $dataArray = json_decode($dataJson, true);

        $room = EntryParser::parse($dataArray['entry']);

        $expected = new Entry('lecture');

        self::assertEquals($expected, $room);
    }
}
