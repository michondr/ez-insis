<?php

namespace App\Tests\TimetableBuilder\Timetable\Action\Subject;

use App\TimetableBuilder\Timetable\Action\Subject\Subject;
use App\TimetableBuilder\Timetable\Action\Subject\SubjectParser;
use PHPUnit\Framework\TestCase;

class SubjectParserTest extends TestCase
{
    public function testParse1()
    {
        $dataJson = '{"subject":{"name":"4EK311 Operations Research","id":"151851","notes":["Day off: 28\/10\/2019"]}}';
        $dataArray = json_decode($dataJson, true);

        $room = SubjectParser::parse($dataArray['subject']);

        $expected = new Subject(151851, '4EK311 Operations Research', ['Day off: 28/10/2019']);

        self::assertEquals($expected, $room);
    }

    public function testParse2()
    {
        $dataJson = '{"subject":{"name":"4IZ268 Web Technologies","id":"151950","notes":[]}}';
        $dataArray = json_decode($dataJson, true);

        $room = SubjectParser::parse($dataArray['subject']);

        $expected = new Subject(151950, '4IZ268 Web Technologies', []);

        self::assertEquals($expected, $room);
    }
}
