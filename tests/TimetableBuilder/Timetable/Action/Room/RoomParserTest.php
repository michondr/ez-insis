<?php

namespace App\Tests\TimetableBuilder\Timetable\Action\Room;

use App\TimetableBuilder\Timetable\Action\Room\Room;
use App\TimetableBuilder\Timetable\Action\Room\RoomParser;
use PHPUnit\Framework\TestCase;

class RoomParserTest extends TestCase
{
    public function testParse1()
    {
        $dataJson = '{"room":{"name":"RB 336","id":"1298"}}';
        $dataArray = json_decode($dataJson, true);

        $room = RoomParser::parse($dataArray['room']);

        $expected = new Room(1298, 'RB 336', 'WCH');

        self::assertEquals($expected, $room);
    }

    public function testParse2()
    {
        $dataJson = '{"room":{"name":"JM 361 (JM)","id":"790"}}';
        $dataArray = json_decode($dataJson, true);

        $room = RoomParser::parse($dataArray['room']);

        $expected = new Room(790, 'JM 361 (JM)', 'JM');

        self::assertEquals($expected, $room);
    }
}
