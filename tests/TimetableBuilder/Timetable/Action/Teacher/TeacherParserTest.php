<?php

namespace App\Tests\TimetableBuilder\Timetable\Action\Teacher;

use App\TimetableBuilder\Timetable\Action\Teacher\Teacher;
use App\TimetableBuilder\Timetable\Action\Teacher\TeacherParser;
use PHPUnit\Framework\TestCase;

class TeacherParserTest extends TestCase
{
    public function testParse1()
    {
        $dataJson = '{"teacher":{"name":"V. P\u00e1nkov\u00e1","id":"1097"}}';
        $dataArray = json_decode($dataJson, true);

        $room = TeacherParser::parse($dataArray['teacher']);

        $expected = new Teacher(1097, 'V. Pánková');

        self::assertEquals($expected, $room);
    }

    public function testParse2()
    {
        $dataJson = '{"teacher":{"name":"S. Horn\u00fd","id":"687"}}';
        $dataArray = json_decode($dataJson, true);

        $room = TeacherParser::parse($dataArray['teacher']);

        $expected = new Teacher(687, 'S. Horný');

        self::assertEquals($expected, $room);
    }
}
