<?php

namespace App\Tests\TimetableBuilder\Timetable\Action;

use App\DateTime\Time;
use App\TimetableBuilder\Timetable\Action\Entry\Entry;
use App\TimetableBuilder\Timetable\Action\Room\Room;
use App\TimetableBuilder\Timetable\Action\Subject\Subject;
use App\TimetableBuilder\Timetable\Action\Teacher\Teacher;
use App\TimetableBuilder\Timetable\Action\TimetableAction;
use App\TimetableBuilder\Timetable\Action\TimetableActionParser;
use PHPUnit\Framework\TestCase;

class TimetableActionParserTest extends TestCase
{
    public function testParse1()
    {
        $dataJson = '{"day":"mon","time_from":"16:15","time_until":"17:45","subject":{"name":"4ME213 3D Graphics","id":"152089","notes":["Day off: 28\/10\/2019"]},"entry":"lecture","room":{"name":"RB 336","id":"1298"},"teacher":{"name":"J. Buben\u00ed\u010dek","id":"54275"},"restriction":"","capacity":"40"}';
        $dataArray = json_decode($dataJson, true);
        $setId = 346;

        $action = TimetableActionParser::parse($setId, $dataArray);

        $expected = new TimetableAction(
            $setId,
            'mon',
            new Time(16, 15),
            new Time(17, 45),
            new Entry('lecture'),
            new Subject(152089, '4ME213 3D Graphics', ['Day off: 28/10/2019']),
            new Room(1298, 'RB 336', 'WCH'),
            new Teacher(54275, 'J. Bubeníček')
        );;

        self::assertEquals($expected, $action);
    }

    public function testParse2()
    {
        $dataJson = '{"day":"tue","time_from":"11:00","time_until":"12:30","subject":{"name":"4DM208 Introduction to the social-economic demography","id":"151889","notes":["Campus: Ji\u017en\u00ed M\u011bsto"]},"entry":"lecture","room":{"name":"JM 252 (JM)","id":"714"},"teacher":{"name":"J. Langhamrov\u00e1","id":"934"},"restriction":"b-sd-1,2","capacity":"72"}';
        $dataArray = json_decode($dataJson, true);
        $setId = 346;

        $action = TimetableActionParser::parse($setId, $dataArray);

        $expected = new TimetableAction(
            $setId,
            'tue',
            new Time(11, 0),
            new Time(12, 30),
            new Entry('lecture'),
            new Subject(151889, '4DM208 Introduction to the social-economic demography', ['Campus: Jižní Město']),
            new Room(714, 'JM 252 (JM)', 'JM'),
            new Teacher(934, 'J. Langhamrová')
        );;

        self::assertEquals($expected, $action);
    }

    public function testParseAll1()
    {
        $dataJson = '{"id":2715,"downloaded":"2019-08-08 22:26:53","last_change":"2019-08-07 10:59:00","validity":{"from":"2019-09-16","until":"2019-12-15"},"timetable":{"name":"Rozvrh prezenční formy - FIS","study_period":"WS 2019/2020","department":"FIS","study_form":"full-time","beginning":"2019-09-16","end":"2019-12-15"},"data":[{"day":"mon","time_from":"08:15","time_until":"17:00","subject":{"name":"Block class","id":"","notes":["Mon 20. 1. 2020 NB 470 - 4ES630 International Week - KEST1, J. FischerMon 20. 1. 2020 NB 471 - 4IT470 International Week - KIT2, T. BrucknerMon 20. 1. 2020 NB 459 - 4IZ573 International Week - KIZI, V. SklenákMon 20. 1. 2020 NB 456 - 4SA630 International Week - KSA 1, P. Doucek"]},"entry":"","room":{"name":"NB 470, NB 456, NB 459, NB 471","id":""},"teacher":{"name":"","id":""},"restriction":"","capacity":""},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4IZ231 Artificial Intelligence and its applications","id":"151947","notes":["Day off: 28/10/2019"]},"entry":"lecture","room":{"name":"SB 305A","id":"5659"},"teacher":{"name":"O. Zamazal","id":"7282"},"restriction":"","capacity":"20"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4EK413 Nonlinear Models","id":"151862","notes":["Day off: 28/10/2019"]},"entry":"lecture","room":{"name":"NB 456","id":"6945"},"teacher":{"name":"V. Pánková","id":"1097"},"restriction":"","capacity":"20"}]}';
        $dataArray = json_decode($dataJson, true)['data'];
        $setId = 346;

        $actions = TimetableActionParser::parseAll($setId, $dataArray);

        $expected = [
            new TimetableAction(
                $setId,
                'mon',
                new Time(9, 15),
                new Time(10, 45),
                new Entry('lecture'),
                new Subject(151947, '4IZ231 Artificial Intelligence and its applications', ['Day off: 28/10/2019']),
                new Room(5659, 'SB 305A', 'WCH'),
                new Teacher(7282, 'O. Zamazal')
            ),
            new TimetableAction(
                $setId,
                'mon',
                new Time(9, 15),
                new Time(10, 45),
                new Entry('lecture'),
                new Subject(151862, '4EK413 Nonlinear Models', ['Day off: 28/10/2019']),
                new Room(6945, 'NB 456', 'WCH'),
                new Teacher(1097, 'V. Pánková')
            ),
        ];

        self::assertEquals($expected, $actions);
    }

//    public function testParseAll2()
//    {
//        $dataJson = '{"id":2715,"downloaded":"2019-08-08 22:26:53","last_change":"2019-08-07 10:59:00","validity":{"from":"2019-09-16","until":"2019-12-15"},"timetable":{"name":"Rozvrh prezenční formy - FIS","study_period":"WS 2019/2020","department":"FIS","study_form":"full-time","beginning":"2019-09-16","end":"2019-12-15"},"data":[{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4ES409 System of National Accounts and Analyses","id":"151921","notes":["Day off: 28/10/2019"]},"entry":"lecture","room":{"name":"RB 106","id":"1205"},"teacher":{"name":"S. Hronová","id":"700"},"restriction":"","capacity":"25"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4EK211 Basic Econometrics","id":"151844","notes":["Day off: 28/10/2019"]},"entry":"seminar","room":{"name":"SB 202","id":"711"},"teacher":{"name":"V. Pánková","id":"1097"},"restriction":"","capacity":"20"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4ME231 Basics of Audiovisual Communication","id":"152093","notes":["Day off: 28/10/2019","Co-teachers: F. Truhlář"]},"entry":"seminar","room":{"name":"RB 336","id":"1298"},"teacher":{"name":"P. Michalik","id":"4639"},"restriction":"","capacity":"16"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4ST231 Elementary Statistics","id":"152135","notes":["Day off: 28/10/2019"]},"entry":"seminar","room":{"name":"SB 204","id":"2282"},"teacher":{"name":"Z. Pourová","id":"1153"},"restriction":"","capacity":"25"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4IT216 Information Systems Analysis and Design","id":"151991","notes":["Day off: 28/10/2019"]},"entry":"seminar","room":{"name":"SB 108","id":"722"},"teacher":{"name":"F. Vencovský","id":"44890"},"restriction":"","capacity":"20"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4EK310 Models of Operations Research","id":"151850","notes":["Day off: 28/10/2019"]},"entry":"seminar","room":{"name":"SB 206","id":"753"},"teacher":{"name":"J. Jablonský","id":"722"},"restriction":"","capacity":"20"},{"day":"mon","time_from":"09:15","time_until":"10:45","subject":{"name":"4ST430 Probability Theory and Mathematical Statistics 2","id":"152159","notes":["Day off: 28/10/2019"]},"entry":"seminar","room":{"name":"SB 209","id":"791"},"teacher":{"name":"A. Čabla","id":"42269"},"restriction":"","capacity":"25"}]}';
//        $dataArray = json_decode($dataJson, true);
//
//        $action = TimetableActionParser::parse($dataArray);
//
//        $expected = new TimetableAction(
//            'tue',
//            new Time(11, 0),
//            new Time(12, 30),
//            new Entry('lecture'),
//            new Subject(151889, '4DM208 Introduction to the social-economic demography', ['Campus: Jižní Město']),
//            new Room(714, 'JM 252 (JM)', 'JM'),
//            new Teacher(934, 'J. Langhamrová')
//        );;
//
//        self::assertEquals($expected, $action);
//    }
}
