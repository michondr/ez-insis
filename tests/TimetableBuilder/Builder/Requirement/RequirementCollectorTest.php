<?php

namespace App\Tests\TimetableBuilder\Builder\Requirement;

use App\TimetableBuilder\Builder\Requirement\Exception\UnknownRequirementException;
use App\TimetableBuilder\Builder\Requirement\RequirementCollector;
use App\TimetableBuilder\Builder\Requirement\RequirementsForAction\SubjectIdRequirement;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class RequirementCollectorTest extends TestCase
{
    /** @var RequirementCollector */
    private $collector;

    public function handleSetRequestTest()
    {
        $inputData = [
            [
                "set" => 2715,
                "requirements" => [
                    "subject_id" => [
                        151844,
                        151985,
                    ],
                ],
            ],
        ];

        $request = new Request([], [], [], [], [], [], json_encode($inputData));

        dump($request->getContent());

        self::assertNotNull(null);
    }

    public function testExisting()
    {
        self::assertInstanceOf(SubjectIdRequirement::class, $this->collector->getRequirementByName('subject_id'));
    }

    public function testNonExisting()
    {
        self::expectException(UnknownRequirementException::class);
        $this->collector->getRequirementByName('subject_name');
    }

    protected function setUp(): void
    {
        $this->collector = new RequirementCollector();
    }
}
