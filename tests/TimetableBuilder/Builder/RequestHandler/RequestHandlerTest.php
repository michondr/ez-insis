<?php

namespace App\Tests\TimetableBuilder\Builder\RequestHandler;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class RequestHandlerTest extends TestCase
{
    public function testHandleSetRequest()
    {
        $inputData = [
            [
                "set" => 2715,
                "requirements" => [
                    "subject_id" => [
                        151844,
                        151985,
                    ],
                ],
            ],
        ];

        $request = new Request([], [], [], [], [], [], json_encode($inputData));

//        dump($this->requestHandler->handleSetRequest($request));

        self::assertNotNull($this->requestHandler->handleSetRequest($request));
    }
}
