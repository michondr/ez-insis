<?php

namespace App\DateTime;

use DateInterval as DateIntervalPhp;
use DateTime as DateTimePhp;

class Date
{
    private $year;
    private $month;
    private $day;

    public function __construct($year, $month, $day)
    {
        $this->year = (int)$year;
        $this->month = (int)$month;
        $this->day = (int)$day;
    }

    public function toMySql()
    {
        return $this->toFormat('Y-m-d');
    }

    public function toTimestamp()
    {
        return mktime(0, 0, 0, $this->month, $this->day, $this->year);
    }

    public function toFormat($format)
    {
        return date($format, $this->toTimestamp());
    }

    public function getDay()
    {
        return $this->day;
    }

    public function getDayOfWeek()
    {
        return (int)date('N', $this->toTimestamp());
    }

    public function getWeek()
    {
        return (int)date('W', $this->toTimestamp());
    }

    public function getMonth()
    {
        return $this->month;
    }

    public function getYear()
    {
        return $this->year;
    }
}
