<?php

namespace App\DateTime;

use DateTime as DateTimePhp;

class DateTimeFactory
{
    const FORMAT_DATE_ONLY = 'Y-m-d';
    const FORMAT_DATETIME = 'Y-m-d H:i:s';

    public static function now()
    {
        return self::fromTimestamp(microtime(true));
    }

    public static function fromStandardSql($sqlString): DateTime
    {
        return self::fromTimestamp(strtotime($sqlString));
    }

    public static function fromFormat($sourceFormat, $dateTimeString)
    {
        $dateTime = DateTimePhp::createFromFormat('!'.$sourceFormat, $dateTimeString);

        return self::fromTimestamp($dateTime->getTimestamp());
    }

    public static function fromTimestamp($timestamp): DateTime
    {
        if (!$timestamp) {
            throw new \InvalidArgumentException();
        }

        return new DateTime(
            new Date(
                date('Y', $timestamp),
                date('m', $timestamp),
                date('d', $timestamp)
            ),
            new Time(
                date('H', $timestamp),
                date('i', $timestamp),
                date('s', $timestamp) + ($timestamp - (int)$timestamp)
            )
        );
    }
}
