<?php

namespace App\DateTime;

use Exception;

class Day
{
    const DAY_MONDAY = 1;
    const DAY_TUESDAY = 2;
    const DAY_WEDNESDAY = 3;
    const DAY_THURSDAY = 4;
    const DAY_FRIDAY = 5;
    const DAY_SATURDAY = 6;
    const DAY_SUNDAY = 7;

    public static function getDayNames()
    {
        return [
            self::DAY_MONDAY => 'Monday',
            self::DAY_TUESDAY => 'Tuesday',
            self::DAY_WEDNESDAY => 'Wednesday',
            self::DAY_THURSDAY => 'Thursday',
            self::DAY_FRIDAY => 'Friday',
            self::DAY_SATURDAY => 'Saturday',
            self::DAY_SUNDAY => 'Sunday',
        ];
    }

    public static function getDayNamesShort()
    {
        return [
            self::DAY_MONDAY => 'mon',
            self::DAY_TUESDAY => 'tue',
            self::DAY_WEDNESDAY => 'wed',
            self::DAY_THURSDAY => 'thu',
            self::DAY_FRIDAY => 'fri',
            self::DAY_SATURDAY => 'sat',
            self::DAY_SUNDAY => 'sun',
        ];
    }

    public static function getDayNamesCzech()
    {
        return [
            self::DAY_MONDAY => 'Pondělí',
            self::DAY_TUESDAY => 'Úterý',
            self::DAY_WEDNESDAY => 'Středa',
            self::DAY_THURSDAY => 'Čtvrtek',
            self::DAY_FRIDAY => 'Pátek',
            self::DAY_SATURDAY => 'Sobota',
            self::DAY_SUNDAY => 'Neděle',
        ];
    }

    public static function getDayName(int $day, bool $cz = false)
    {
        if ($cz) {
            $name = self::getDayNamesCzech()[$day];
        } else {
            $name = self::getDayNames()[$day];
        }

        if (is_null($name)) {
            throw new Exception('Unsupported day');
        }

        return $name;
    }

    public static function getDayNum(string $day)
    {
        if ($day === 'Po') {
            return self::DAY_MONDAY;
        }
        if ($day === 'Út') {
            return self::DAY_TUESDAY;
        }
        if ($day === 'St') {
            return self::DAY_WEDNESDAY;
        }
        if ($day === 'Čt') {
            return self::DAY_THURSDAY;
        }
        if ($day === 'Pá') {
            return self::DAY_FRIDAY;
        }
        if ($day === 'So') {
            return self::DAY_SATURDAY;
        }
        if ($day === 'Ne') {
            return self::DAY_SUNDAY;
        }
        throw new Exception('Unsupported day');
    }
}
