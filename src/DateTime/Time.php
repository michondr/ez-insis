<?php

namespace App\DateTime;

use DateTime as DateTimePhp;

class Time
{
    private $hour;
    private $minute;
    private $second;

    public function __construct($hour, $minute, $second = 0)
    {
        $this->hour = (int)$hour;
        $this->minute = (int)$minute;
        $this->second = (int)$second;
    }

    public static function fromHI(string $time)
    {
        $timestamp = DateTimePhp::createFromFormat('H:i', $time)->getTimestamp();

        return new Time(
            date('H', $timestamp),
            date('i', $timestamp),
            date('s', $timestamp)
        );
    }

    public function toHI()
    {
        return $this->toFormat('H:i');
    }

    public function toTimestamp(): int
    {
        return ($this->second) + ($this->minute * 60) + ($this->hour * 60 * 60);
    }

    public function getHour()
    {
        return $this->hour;
    }

    public function getMinute()
    {
        return $this->minute;
    }

    public function getSecond()
    {
        return $this->second;
    }

    private function toFormat($format)
    {
        return date(
            $format,
            mktime(
                $this->hour,
                $this->minute,
                $this->second
            )
        );
    }
}
