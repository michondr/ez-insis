<?php

namespace App\Cache;

use Symfony\Component\Cache\Simple\FilesystemCache;

class CacheData extends FilesystemCache
{
    const TTL = 3600;
    
    public function __construct()
    {
        parent::__construct('route_limit');
    }

    public static function keySet()
    {
        return 'TT-set';
    }

    public static function keyItem(int $id, string $lang)
    {
        return 'TT-item'.$id.$lang;
    }
}
