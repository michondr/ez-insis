<?php

namespace App\Cache;

use Symfony\Component\Cache\Simple\FilesystemCache;

class CacheRouteLimit extends FilesystemCache
{
    public function __construct()
    {
        parent::__construct('data_cache');
    }
}
