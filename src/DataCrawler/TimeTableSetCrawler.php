<?php

namespace App\DataCrawler;

use App\Cache\CacheData;
use App\DateTime\DateTimeFactory;
use App\Http\BrowserDataProvider;
use App\Http\Client;
use App\Http\Client\Config;
use App\Http\DomFactory;

class TimeTableSetCrawler
{
    private $client;
    private $cache;

    public function __construct(
        Client $client,
        CacheData $cache
    ) {
        $this->client = $client;
        $this->cache = $cache;
    }

    public function crawlItemOrReturnCached()
    {
        $key = CacheData::keySet();

        if ($this->cache->has($key)) {
            $data = $this->cache->get($key);
        } else {
            $data = $this->scrape();
            $this->cache->set($key, $data, CacheData::TTL);
        }

        return $data;
    }

    private function scrape()
    {
        $data = [];
        $response = $this->client->downloadPage($this->prepareConfig());
        $html = $response->getLastResponse()->getBody();
//        $html = file_get_contents('https://s3.eu-central-1.amazonaws.com/michondr/time-table-tweak-json/timetable_data.html');

        $dom = DomFactory::createDom($html);
        $rows = $dom->query('//table/tbody/tr');

        /** @var \DOMElement $row */
        foreach ($rows as $row) {
            $rowData = [];

            $id = $dom->query('./td[1]//input/@value', $row)[0]->nodeValue;
            $rowData['name'] = $dom->query('./td[2]', $row)[0]->nodeValue;
            $rowData['study_period'] = $dom->query('./td[3]', $row)[0]->nodeValue;
            $rowData['department'] = $dom->query('./td[4]', $row)[0]->nodeValue;
            $rowData['study_form'] = $dom->query('./td[5]', $row)[0]->nodeValue;
            $rowData['beginning'] = DateTimeFactory::fromFormat('d/m/Y', $dom->query('./td[6]', $row)[0]->nodeValue)->toFormat('Y-m-d');
            $rowData['end'] = DateTimeFactory::fromFormat('d/m/Y', $dom->query('./td[7]', $row)[0]->nodeValue)->toFormat('Y-m-d');

            $data[$id] = $rowData;
        }

        krsort($data);

        $set = [
            'downloaded' => DateTimeFactory::now()->toMySql(),
            'data' => $data,
        ];

        return $set;
    }

    private function prepareConfig(): Config
    {
        $url = 'https://insis.vse.cz/katalog/rozvrhy_view.pl?konf=1;f=-1';

        $config = new Config($url);
        $config->setEncoding('gzip, deflate');
        $config->setUserAgent(BrowserDataProvider::getUserAgent());

        return $config;
    }
}
