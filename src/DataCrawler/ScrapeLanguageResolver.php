<?php

namespace App\DataCrawler;

use App\DataCrawler\Exception\UnsupportedLangException;
use Symfony\Component\HttpFoundation\Request;

class ScrapeLanguageResolver
{
    const ALLOWED_LANGUAGES = ['cz', 'en'];

    public static function resolveLanguage(Request $request)
    {
        $lang = $request->get('lang', 'en');

        if (!in_array($lang, self::ALLOWED_LANGUAGES)) {
            throw new UnsupportedLangException($lang);
        }

        return $lang;
    }
}
