<?php

namespace App\DataCrawler;

use App\Cache\CacheData;
use App\DateTime\DateTimeFactory;
use App\Http\BrowserDataProvider;
use App\Http\Client;
use App\Http\Client\Config;
use App\Http\Client\PostDataArray;
use App\Http\DomFactory;

class TimeTableItemCrawler
{
    private $client;
    private $cache;
    private $timeTableSetCrawler;

    public function __construct(
        Client $client,
        CacheData $cache,
        TimeTableSetCrawler $timeTableSetCrawler
    ) {
        $this->client = $client;
        $this->cache = $cache;
        $this->timeTableSetCrawler = $timeTableSetCrawler;
    }

    public function crawlItemOrReturnCached(int $id, string $lang)
    {
        $key = CacheData::keyItem($id, $lang);

        if ($this->cache->has($key)) {
            $data = $this->cache->get($key);
        } else {
            $data = $this->scrape($id, $lang);
            $this->cache->set($key, $data, CacheData::TTL);
        }

        return $data;
    }

    private function scrape(int $id, string $lang)
    {
        $response = $this->client->downloadPage($this->prepareConfig($id, $lang));
        $html = $response->getLastResponse()->getBody();
//        $html = file_get_contents('https://s3.eu-central-1.amazonaws.com/michondr/time-table-tweak-json/timetable_item_data.html');
//        $html = file_get_contents('/home/michondr/Desktop/check.html');
//        $html = file_get_contents('/home/michondr/Desktop/check_en.html');

        if (strpos($html, 'No class match the selected criteria.') !== false) {
            return [];
        }

        $dom = DomFactory::createDom($html);
        $info = $this->getInfo($html, $lang);

        $timetable = [
            'id' => $id,
            'downloaded' => $info['downloaded'],
            'last_change' => $info['last_change'],
            'validity' => $info['validity'],
            'timetable' => $this->timeTableSetCrawler->crawlItemOrReturnCached()['data'][$id] ?? [],
            'data' => $this->getData($dom),
        ];

        return $timetable;
    }

    private function prepareConfig(int $id, string $lang): Config
    {
        $url = 'https://insis.vse.cz/katalog/rozvrhy_view.pl';

        $config = new Config($url);
        $config->setEncoding('gzip, deflate');
        $config->setUserAgent(BrowserDataProvider::getUserAgent());
        $config->setPostData(
            new PostDataArray(
                [
                    'rozvrh' => $id,
                    'format' => 'list',
                    'zobraz' => 'Display',
                    'lang' => $lang,
                ]
            )
        );

        return $config;
    }

    private function getInfo(string $html, string $lang)
    {
        //can't crawl from dom by xpath :((

        if ($lang === 'en') {
            $matcherValidity = 'Validity: ([0-9]+/[0-9]+/[0-9]+) - ([0-9]+/[0-9]+/[0-9]+)';
            $matcherLastChange = 'Last change: ([0-9]+/[0-9]+/[0-9]+) ([0-9]+:[0-9]+)';
            $formatValidity = 'd/m/Y';
            $formatLastChange = 'd/m/Y H:i';
        } elseif ($lang === 'cz') {
            $matcherValidity = 'Platnost: ([0-9]+\. [0-9]+\. [0-9]+) - ([0-9]+\. [0-9]+\. [0-9]+)';
            $matcherLastChange = 'Poslední změna: ([0-9]+\. [0-9]+\. [0-9]+) ([0-9]+:[0-9]+)';
            $formatValidity = 'd. m. Y';
            $formatLastChange = 'd. m. Y H:i';
        } else {
            throw new \Exception('not here');
        }

        preg_match_all('~'.$matcherValidity.'~', $html, $matchesValidity);
        preg_match_all('~'.$matcherLastChange.'~', $html, $matchesLastChange);

        return [
            'validity' => [
                'from' => DateTimeFactory::fromFormat($formatValidity, $matchesValidity[1][0])->toFormat('Y-m-d'),
                'until' => DateTimeFactory::fromFormat($formatValidity, $matchesValidity[2][0])->toFormat('Y-m-d'),
            ],
            'last_change' => DateTimeFactory::fromFormat($formatLastChange, $matchesLastChange[1][0].' '.$matchesLastChange[2][0])->toMySql(),
            'downloaded' => DateTimeFactory::now()->toMySql(),
        ];
    }

    private function getData(\DOMXPath $dom)
    {
        $data = [];
        $notes = [];

        $rows = $dom->query('//div/table[1]/tbody/tr');
        $notesRows = $dom->query('//div/table[2]/tbody/tr/td[2]');

        /** @var \DOMElement $notesRow */
        foreach ($notesRows as $notesRow) {
            $notes[] = $notesRow->nodeValue;
        }

        /** @var \DOMElement $row */
        foreach ($rows as $index => $row) {
            $rowData = [];

            $rowData['day'] = $this->getDay($dom, $row);
            $rowData['time_from'] = $this->getTimeFrom($dom, $row);
            $rowData['time_until'] = $this->getTimeUntil($dom, $row);
            $rowData['subject'] = $this->getSubject($dom, $row, $notes);
            $rowData['entry'] = $this->getEntry($dom, $row);
            $rowData['room'] = $this->getRoom($dom, $row);
            $rowData['teacher'] = $this->getTeacher($dom, $row);
            $rowData['restriction'] = $this->getRestriction($dom, $row);
            $rowData['capacity'] = $this->getCapacity($dom, $row);

            $data[] = $rowData;
        }

        return $data;
    }

    private function getDay(\DOMXPath $dom, \DOMElement $row)
    {
        $day = $dom->query('./td[1]', $row)[0]->nodeValue;

        return strtolower($day);
    }

    private function getTimeFrom(\DOMXPath $dom, \DOMElement $row)
    {
        return $dom->query('./td[2]', $row)[0]->nodeValue;
    }

    private function getTimeUntil(\DOMXPath $dom, \DOMElement $row)
    {
        return $dom->query('./td[3]', $row)[0]->nodeValue;
    }

    private function getSubject(\DOMXPath $dom, \DOMElement $row, array $notes): array
    {
        $name = $dom->query('./td[4]', $row)[0]->nodeValue;
        $name = str_replace("\xc2\xa0", ' ', $name); //removing nbsp
        $href = $dom->query('./td[4]//@href', $row)[0];

        $nameWithoutNotes = preg_replace('~\(.*?\)~', '', $name);
        $noteIdsString = $this->extractRegex($name, '\((.*?)\)');
        $noteIds = explode(',', str_replace(' ', '', $noteIdsString));

        return [
            'name' => trim($nameWithoutNotes),
            'id' => $this->extractRegex(is_null($href) ? '' : $href->nodeValue, 'predmet=([0-9]+)'),
            'notes' => $this->matchNotes($noteIds, $notes),
        ];
    }

    private function getEntry(\DOMXPath $dom, \DOMElement $row)
    {
        $entry = $dom->query('./td[5]', $row)[0]->nodeValue;

        return strtolower($entry);
    }

    private function getRoom(\DOMXPath $dom, \DOMElement $row): array
    {
        $href = $dom->query('./td[6]//@href', $row)[0];

        return [
            'name' => $dom->query('./td[6]', $row)[0]->nodeValue,
            'id' => $this->extractRegex(is_null($href) ? '' : $href->nodeValue, 'zobrazit_mistnost=([0-9]+)'),
        ];
    }

    private function getTeacher(\DOMXPath $dom, \DOMElement $row): array
    {
        $href = $dom->query('./td[7]//@href', $row)[0];

        return [
            'name' => $dom->query('./td[7]', $row)[0]->nodeValue,
            'id' => $this->extractRegex(is_null($href) ? '' : $href->nodeValue, 'id=([0-9]+)'),
        ];
    }

    private function getRestriction(\DOMXPath $dom, \DOMElement $row)
    {
        return $dom->query('./td[8]', $row)[0]->nodeValue;
    }

    private function getCapacity(\DOMXPath $dom, \DOMElement $row)
    {
        return $dom->query('./td[9]', $row)[0]->nodeValue;
    }

    private function extractRegex(string $source, string $matcher): string
    {
        preg_match('~'.$matcher.'~', $source, $match);

        return $match[1] ?? '';
    }

    private function matchNotes(array $noteIds, $notes): array
    {
        if (empty($noteIds)) {
            return [];
        }

        $matchedNotes = [];

        foreach ($noteIds as $id) {
            if (key_exists($id, $notes)) {
                $matchedNotes[] = $notes[$id - 1];
            }
        }

        return $matchedNotes;
    }
}