<?php

namespace App\DataCrawler\Exception;

use App\DataCrawler\ScrapeLanguageResolver;
use App\Response\Json\Exception\AllowedException;

class UnsupportedLangException extends AllowedException
{
    public function __construct(string $lang)
    {
        $supported = implode(',', ScrapeLanguageResolver::ALLOWED_LANGUAGES);

        parent::__construct('lang not supported: '.$lang.' supported languages: '.$supported);
    }
}
