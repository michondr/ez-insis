<?php

namespace App\Controller;

use App\Cache\CacheData;
use App\DataCrawler\ScrapeLanguageResolver;
use App\DataCrawler\TimeTableItemCrawler;
use App\DataCrawler\TimeTableSetCrawler;
use App\Filter\Filter;
use App\Response\Json\EzJsonResponseCorrect;
use App\Response\Json\EzJsonResponseMissing;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DataController extends Controller
{
    const DATA_TTL = 3600;

    private $setCrawler;
    private $itemCrawler;

    public function __construct(
        TimeTableSetCrawler $setCrawler,
        TimeTableItemCrawler $itemCrawler
    ) {
        $this->setCrawler = $setCrawler;
        $this->itemCrawler = $itemCrawler;
    }

    /**
     * @Route("/data/time-table/set", name="data.time-table.set", methods={"GET"})
     */
    public function timeTableSetAction(Request $request)
    {
        $data = $this->setCrawler->crawlItemOrReturnCached();

        $filter = new Filter($request, $data, ['name', 'study_period', 'department', 'study_form', 'beginning', 'end']);

        if (!$filter->isEmpty()) {
            $data = $filter->getInOutputFormat();
        }

        return new EzJsonResponseCorrect($data);
    }

    /**
     * @Route("/data/time-table/item", name="data.time-table.item", methods={"GET"})
     */
    public function timeTableItemAction(Request $request)
    {
        $timeTableItemId = $request->query->get('time_table_item_id');

        if(is_null($timeTableItemId)){
            return new EzJsonResponseMissing(['time_table_item_id']);
        }

        $data = $this->itemCrawler->crawlItemOrReturnCached($timeTableItemId, ScrapeLanguageResolver::resolveLanguage($request));

        $filter = new Filter($request, $data, ['day', 'time_from', 'time_until', 'entry', 'teacher_id', 'teacher_name', 'subject_id', 'subject_name', 'room']);

        if (!$filter->isEmpty()) {
            $data = $filter->getInOutputFormat();
        }

        return new EzJsonResponseCorrect($data);
    }
}
