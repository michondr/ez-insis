<?php

namespace App\Controller;

use App\Cache\CacheData;
use App\Cache\CacheRouteLimit;
use App\Logging\ExceptionLogger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DevController extends Controller
{
    private $exceptionLogger;
    private $cacheData;
    private $cacheRouteLimit;

    public function __construct(
        ExceptionLogger $exceptionLogger,
        CacheData $cacheData,
        CacheRouteLimit $cacheRouteLimit
    ) {
        $this->exceptionLogger = $exceptionLogger;
        $this->cacheData = $cacheData;
        $this->cacheRouteLimit = $cacheRouteLimit;
    }

    /**
     * @Route("/dev", name="dev")
     */
    public function dev(Request $request)
    {
        if ($request->query->has('cache')) {
            if ($request->query->get('cache') === 'data') {
                $this->cacheData->clear();
            }
            if ($request->query->get('cache') === 'limit') {
                $this->cacheRouteLimit->clear();
            }
        }

        return new JsonResponse(['message' => 'hello world!']);
    }
}
