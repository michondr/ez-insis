<?php

namespace App\Controller;

use App\AccessControl\AccessToken\AccessTokenGenerator;
use App\Logging\ExceptionLogger;
use Sentry\SentryBundle\SentrySymfonyClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccessTokenController extends Controller
{
    private $exceptionLogger;

    public function __construct(
        ExceptionLogger $exceptionLogger
    ) {
        $this->exceptionLogger = $exceptionLogger;
    }

    /**
     * @Route("/access/token-request", name="access.token-request", methods={"GET"})
     */
    public function tokenRequestAction(Request $request)
    {
        $email = $request->query->get('email');

        if (is_null($email)) {
            return new JsonResponse(
                ['result' => 'Hey, specify some mail in "email" parameter so I can get in touch with you'],
                Response::HTTP_BAD_REQUEST
            );
        }

        $this->exceptionLogger->log('new token request', ['email' => $email], SentrySymfonyClient::FATAL);

        return new JsonResponse(
            ['result' => 'I\'ve received your request with email "'.$email.'" and will be processing it soon'],
            Response::HTTP_OK
        );
    }

    /**
     * @Route("/access/token-generate", name="access.token-generate", methods={"GET"})
     */
    public function tokenGenerateAction()
    {
        return new JsonResponse(['accessToken' => AccessTokenGenerator::generateToken()]);
    }
}
