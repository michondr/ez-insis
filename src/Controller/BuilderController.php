<?php

namespace App\Controller;

use App\Response\Json\EzJsonResponseCorrect;
use App\Response\Json\EzJsonResponseError;
use App\TimetableBuilder\Builder\RequestHandler\RequestHandler;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BuilderController extends Controller
{
    private $requestHandler;

    public function __construct(
        RequestHandler $requestHandler
    ) {
        $this->requestHandler = $requestHandler;
    }

    /**
     * @Route("/builder/time-table/set", name="builder.time-table.set", methods={"POST"})
     */
    public function builderTimeTableSetAction(Request $request)
    {
        try {
            $data = $this->requestHandler->handleSetRequest($request);
        } catch (Exception $e) {
            return new EzJsonResponseError($e);
        }

        return new EzJsonResponseCorrect($data);
    }

    /**
     * @Route("/builder/time-table/get/{buildHash}", name="builder.time-table.get", methods={"POST"})
     */
    public function builderTimeTableGetAction(Request $request, int $buildHash)
    {
        try {
            $data = $this->requestHandler->handleGetRequest($request, $buildHash);
        } catch (Exception $e) {
            return new EzJsonResponseError($e);
        }

        return new EzJsonResponseCorrect($data);
    }
}
