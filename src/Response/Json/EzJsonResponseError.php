<?php

namespace App\Response\Json;

use App\Response\Json\Exception\AllowedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EzJsonResponseError extends JsonResponse
{
    public function __construct(\Exception $exception)
    {
        if (isEnvDev()) {
            parent::__construct(self::getDevMessageForException($exception), Response::HTTP_OK);
        } elseif ($exception instanceof AllowedException) {
            parent::__construct(self::getMessageForException($exception), Response::HTTP_OK);
        } else {
            parent::__construct(self::getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private static function getMessage()
    {
        return [
            'message' => 'internal error occurred. that sucks.... but - I already know what went wrong ;)',
        ];
    }

    private static function getMessageForException(\Exception $exception)
    {
        return [
            'message' => $exception->getMessage(),
        ];
    }

    private static function getDevMessageForException(\Exception $exception)
    {
        return [
            'class' => get_class($exception),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $exception->getTrace(),
        ];
    }
}
