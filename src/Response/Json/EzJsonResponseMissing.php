<?php

namespace App\Response\Json;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EzJsonResponseMissing extends JsonResponse
{
    public function __construct(array $missingFields)
    {
        parent::__construct(['message' => 'these fields are missing: '. implode(', ', $missingFields)], Response::HTTP_BAD_REQUEST);
    }
}
