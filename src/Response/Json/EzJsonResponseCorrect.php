<?php

namespace App\Response\Json;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EzJsonResponseCorrect extends JsonResponse
{
    public function __construct(array $data)
    {
        parent::__construct($data, empty($data) ? Response::HTTP_NO_CONTENT : Response::HTTP_OK);
    }
}
