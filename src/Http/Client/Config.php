<?php

namespace App\Http\Client;

class Config
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    const METHOD_OPTIONS = 'OPTIONS';

    private $url;
    private $userAgent;
    private $timeout;
    private $connectionTimeout;
    private $sslCertificateValidation;
    private $responseBodySizeLimit;
    private $cookiesStorageFile;
    private $maxRedirects;
    private $postData;
    private $method;
    private $proxyConfig;
    private $logFilePath;
    private $credentials;
    private $headers;
    private $encoding;

    public function __construct($url, $method = self::METHOD_GET)
    {
        $this->url = $url;
        $this->sslCertificateValidation = true;
        $this->method = $method;
        $this->timeout = 45;
        $this->connectionTimeout = 30;
        $this->headers = [];
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function setUserAgent(string $userAgent)
    {
        $this->userAgent = $userAgent;
    }

    public function getTimeout()
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout)
    {
        $this->timeout = $timeout;
    }

    public function getConnectionTimeout()
    {
        return $this->connectionTimeout;
    }

    public function setConnectionTimeout(int $connectionTimeout)
    {
        $this->connectionTimeout = $connectionTimeout;
    }

    public function getCookiesStorageFile()
    {
        return $this->cookiesStorageFile;
    }

    public function setCookiesStorageFile($cookiesStorageFile)
    {
        $this->cookiesStorageFile = $cookiesStorageFile;
    }

    public function getMaxRedirects(): ?int
    {
        return $this->maxRedirects;
    }

    public function setMaxRedirects(int $maxRedirects)
    {
        // -1 is unlimited
        $this->maxRedirects = $maxRedirects;
    }

    public function getPostData(): ?IPostData
    {
        return $this->postData;
    }

    public function setPostData(IPostData $postData, $method = self::METHOD_POST)
    {
        $this->postData = $postData;
        $this->method = $method;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method)
    {
        $this->method = strtoupper($method);
    }

    public function getProxyConfig(): ?ProxyConfig
    {
        return $this->proxyConfig;
    }

    public function setProxyConfig(ProxyConfig $proxyConfig = null)
    {
        $this->proxyConfig = $proxyConfig;
    }

    public function disableSslCertificateValidation()
    {
        $this->sslCertificateValidation = false;
    }

    public function getSslCertificateValidation()
    {
        return $this->sslCertificateValidation;
    }

    public function getResponseBodySizeLimit()
    {
        return $this->responseBodySizeLimit;
    }

    public function setResponseBodySizeLimit($responseBodySizeLimit)
    {
        $this->responseBodySizeLimit = $responseBodySizeLimit;
    }

    public function setHeader($name, $value, $overwrite = false)
    {
        if (array_key_exists($name, $this->headers) && $overwrite === false) {
            throw new \Exception('Header "'.$name.'" already defined');
        }

        $this->headers[$name] = $value;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders(array $headers, $overwrite = false)
    {
        foreach ($headers as $name => $value) {
            $this->setHeader($name, $value, $overwrite);
        }
    }

    public function hasHeaders()
    {
        return count($this->headers) > 0;
    }

    public function getLogFilePath()
    {
        return $this->logFilePath;
    }

    public function setLogFilePath($logFilePath)
    {
        $this->logFilePath = $logFilePath;
    }

    public function getCredentials(): ?ConfigCredentials
    {
        return $this->credentials;
    }

    public function setCredentials(ConfigCredentials $configCredentials)
    {
        $this->credentials = $configCredentials;
    }

    public function disableHttps()
    {
        $this->url = str_replace('https://', 'http://', $this->url);
    }

    public function getEncoding(): ?string
    {
        return $this->encoding;
    }

    public function setEncoding(string $encoding)
    {
        $this->encoding = $encoding;
    }
}
