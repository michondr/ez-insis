<?php

namespace App\Http\Client;

class ResponsesContainer
{
    private $responses;
    private $requestInfo;

    /**
     * @param Response[] $responses
     */
    public function __construct(array $responses, RequestInfo $requestInfo)
    {
        $this->responses = $responses;
        $this->requestInfo = $requestInfo;
    }

    public function getLastResponse()
    {
        $lastIndex = count($this->responses) - 1;

        return $this->responses[$lastIndex];
    }

    public function getResponseCount()
    {
        return count($this->responses);
    }

    public function getRequestInfo()
    {
        return $this->requestInfo;
    }
}
