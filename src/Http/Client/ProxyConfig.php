<?php

namespace App\Http\Client;

class ProxyConfig
{
    private $url;
    private $port;
    private $username;
    private $password;

    public function __construct($url, $port, $username = null, $password = null)
    {
        $this->url = $url;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getProxyUrl(): string
    {
        try {
            if (strpos($this->getUrl(), 'http')) {
                preg_match('~^(https?)://(.*?)$~', $this->getUrl(), $matches);
                $scheme = $matches[1];
                $host = $matches[2];
            } else {
                $scheme = 'http';
                $host = $this->getUrl();
            }

            if ($this->isAuthorised()) {
                return $scheme.'://'.$host.':'.$this->getPort();
            } else {
                return $scheme.'://'.$this->getUsername().':'.$this->getPassword().'@'.$host.':'.$this->getPort();
            }

        } catch (\Exception $e) {
            throw new \Exception('unable to parse url '.$this->getUrl());
        }
    }

    public function isAuthorised()
    {
        return empty($this->username) && empty($this->password);
    }
}
