<?php

namespace App\Http\Client;

class RequestInfo
{
    private $finalUrl;
    private $loadTime;

    public function __construct($finalUrl, $loadTime)
    {
        $this->finalUrl = $finalUrl;
        $this->loadTime = $loadTime;
    }

    public static function create(array $curlInfo)
    {
        return new RequestInfo(
            $curlInfo['url'],
            $curlInfo['total_time']
        );
    }

    public function getFinalUrl()
    {
        return $this->finalUrl;
    }

    public function getLoadTime()
    {
        return $this->loadTime;
    }
}
