<?php

namespace App\Http\Client;

interface IPostData
{
    public function getRawPostDataString();
}
