<?php

namespace App\Http;

use App\Http\Client\Config;
use App\Http\Client\CurlResourceFactory;
use App\Http\Exception\RequestFailedException;
use App\Http\Client\RequestInfo;
use App\Http\Client\ResponseParser;
use App\Http\Client\ResponsesContainer;

class Client
{
    private $responseParser;
    private $curlResourceFactory;

    public function __construct(
        CurlResourceFactory $curlResourceFactory,
        ResponseParser $responseParser
    ) {
        $this->responseParser = $responseParser;
        $this->curlResourceFactory = $curlResourceFactory;
    }

    public function downloadPage(Config $config, bool $disableProxyForRequest = false)
    {
        $proxyCfg = $config->getProxyConfig();
        if ($disableProxyForRequest) {
            $config->setProxyConfig(null);
        }

        $curlResource = $this->curlResourceFactory->create($config);

        $config->setProxyConfig($proxyCfg);

        if ($config->getLogFilePath()) {
            curl_setopt($curlResource, CURLOPT_VERBOSE, true);

            $logHandler = fopen('php://temp', 'rw+');
            curl_setopt($curlResource, CURLOPT_STDERR, $logHandler);
        }

        if ($config->getResponseBodySizeLimit()) {
            $manuallyWrittenData = '';
            $this->curlResourceFactory->handleResponseBodySizeLimit($curlResource, $manuallyWrittenData, $config->getResponseBodySizeLimit());
        }

        if (isset($manuallyWrittenData)) {
            curl_exec($curlResource);
            $data = $manuallyWrittenData;
        } else {
            $data = curl_exec($curlResource);
        }

        $curlInfo = curl_getinfo($curlResource);
        $curlErrorMessage = curl_error($curlResource);
        $curlErrorCode = curl_errno($curlResource);

        curl_close($curlResource);

        if (isset($logHandler) && $config->getLogFilePath()) {
            rewind($logHandler);
            $logData = stream_get_contents($logHandler);

            file_put_contents($config->getLogFilePath(), $logData.PHP_EOL, FILE_APPEND);
        }

        if (!$data) {
            throw new RequestFailedException($curlErrorMessage, $curlErrorCode);
        }

        $responses = $this->responseParser->extract($data);
        $requestInfo = RequestInfo::create($curlInfo);

        return new ResponsesContainer($responses, $requestInfo);
    }
}
