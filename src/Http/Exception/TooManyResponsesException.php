<?php

namespace App\Http\Exception;

class TooManyResponsesException extends \Exception
{
}
