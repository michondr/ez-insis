<?php

namespace App\Http;

class DomFactory
{
    public static function createDom($pageHtml): \DOMXPath
    {
        $doc = new \DOMDocument();
        @$doc->loadHTML($pageHtml);

        return new \DOMXPath($doc);
    }
}
