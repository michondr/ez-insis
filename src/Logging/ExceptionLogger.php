<?php

namespace App\Logging;

use Psr\Container\ContainerInterface;
use Sentry\SentryBundle\SentrySymfonyClient;

class ExceptionLogger
{
    private $sentrySymfonyClient;

    public function __construct(
        ContainerInterface $container
    ) {
        $this->sentrySymfonyClient = $container->get('sentry.client');
    }

    public function log(string $message, array $data = [], string $level = SentrySymfonyClient::INFO)
    {
        $sentryData = [];

        $sentryData['extra'] = $data;
        $sentryData['message'] = $message;
        $sentryData['level'] = $level;

        $this->sentrySymfonyClient->capture($sentryData);
    }

    public function logException(\Exception $exception)
    {
        $this->sentrySymfonyClient->captureException($exception);
    }
}