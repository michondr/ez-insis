<?php

namespace App\TimetableBuilder\Timetable\Score;

use App\TimetableBuilder\Timetable\Timetable;

class ScoreByFreeDays implements IScoreInterface
{
    //by free days in week
    //the higher, the better

    public static function count(Timetable $timetable, int $inputActionsCount): float
    {
        $days = $timetable->getDays();
        $freeDays = [];

        foreach ($days as $dayName => $day) {
            if (empty($day)) {
                $freeDays[] = $dayName;
            }
        }

        //removing weekend. timetable can have actions on weekend, but its not usual
        return ((count($freeDays) - 2) / (count($days) - 2));
    }
}
