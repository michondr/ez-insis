<?php

namespace App\TimetableBuilder\Timetable\Score;

use App\DateTime\Time;
use App\TimetableBuilder\Timetable\Timetable;

class ScoreByLearnedTime implements IScoreInterface
{
    //time spent learning in day, not having breaks. the higher, the better.
    //sum of whole day vs sum of time spend learning

    public static function count(Timetable $timetable, int $inputActionsCount): float
    {
        $dayScores = [];

        foreach ($timetable->getDays() as $dayName => $day) {
            if (empty($day)) {
                continue;
            }

            $dayKeys = array_keys($day);
            $fromValues = [];
            $toValues = [];
            $timesTaken = [];

            foreach ($dayKeys as $dayKey) {
                $exploded = explode('-', $dayKey);
                $from = Time::fromHI($exploded[0])->toTimestamp();
                $to = Time::fromHI($exploded[1])->toTimestamp();

                $fromValues[] = $from;
                $toValues[] = $to;
                $timesTaken[] = ($to - $from);
            }

            $from = min($fromValues);
            $to = max($toValues);
            $dayTakes = ($to - $from);
            $dayIsLearnedFor = array_sum($timesTaken);

            $dayScores[$dayName] = ($dayIsLearnedFor / $dayTakes);
        }

        return (array_sum($dayScores) / count($dayScores));
    }
}
