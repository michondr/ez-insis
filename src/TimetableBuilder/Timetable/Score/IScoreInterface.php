<?php

namespace App\TimetableBuilder\Timetable\Score;

use App\TimetableBuilder\Timetable\Timetable;

interface IScoreInterface
{
    public static function count(Timetable $timetable, int $inputActionsCount): float;
}
