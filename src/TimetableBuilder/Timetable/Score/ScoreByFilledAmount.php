<?php

namespace App\TimetableBuilder\Timetable\Score;

use App\TimetableBuilder\Timetable\Timetable;

class ScoreByFilledAmount implements IScoreInterface
{
    //the higher the better.
    // amount of options at the start of filling the timetables, vs amount of action in timetable

    public static function count(Timetable $timetable, int $inputActionsCount): float
    {
        $actionsInTimetable = $timetable->getActionsCount();

        return ($actionsInTimetable / $inputActionsCount);
    }
}
