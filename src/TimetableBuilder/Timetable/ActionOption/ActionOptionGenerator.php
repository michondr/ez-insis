<?php

namespace App\TimetableBuilder\Timetable\ActionOption;

use App\TimetableBuilder\Timetable\Action\TimetableAction;

class ActionOptionGenerator
{
    public function sort(array $setsWithActions): array
    {
        $sorted = [];

        foreach ($setsWithActions as $setId => $setActions) {
            /** @var TimetableAction $action */
            foreach ($setActions as $action) {
                $actionOption = new ActionOption($setId, $action->getSubject()->getId());
                $key = $actionOption->getKey();

                if (isset($sorted[$key])) {
                    $actionOption = $sorted[$key];
                }

                if ($action->getEntry()->isLecture()) {
                    $actionOption->addLecture($action);
                } elseif ($action->getEntry()->isSeminar()) {
                    $actionOption->addSeminar($action);
                } else {
                    throw new \Exception('unknown entry type: '.$action->getEntry()->getType());
                }

                $sorted[$key] = $actionOption;
            }
        }

        return $sorted;
    }
}
