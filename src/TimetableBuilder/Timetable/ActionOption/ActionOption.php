<?php

namespace App\TimetableBuilder\Timetable\ActionOption;

use App\TimetableBuilder\Timetable\Action\TimetableAction;

class ActionOption
{
    private $setId;
    private $subjectId;
    /** @var TimetableAction[] */
    private $lectures;
    /** @var TimetableAction[] */
    private $seminars;

    public function __construct(
        int $setId,
        int $subjectId
    ) {
        $this->setId = $setId;
        $this->subjectId = $subjectId;
        $this->lectures = [];
        $this->seminars = [];
    }

    public function getKey(): string
    {
        return $this->setId.'->'.$this->subjectId;
    }

    public function addLecture(TimetableAction $action)
    {
        $this->lectures[] = $action;
    }

    public function addSeminar(TimetableAction $action)
    {
        $this->seminars[] = $action;
    }

    public function getEntriesCount(): int
    {
        return ((int)!empty($this->lectures)) + (int)!empty($this->seminars);
    }

    public function getAllPossibleVersions(): array
    {
        $out = [];

        foreach ($this->lectures as $lecture) {
            foreach ($this->seminars as $seminar) {
                $out[] = [
                    'lecture' => $lecture,
                    'seminar' => $seminar,
                ];
            }
        }

        if (!empty($out)) {
            return $out;
        }

        if (empty($out) and !empty($this->lectures)) {
            foreach ($this->lectures as $lecture) {
                $out[] = [
                    'lecture' => $lecture,
                    'seminar' => null,
                ];

            }
        }

        if (empty($out) and !empty($this->seminars)) {
            foreach ($this->seminars as $seminar) {
                $out[] = [
                    'lecture' => null,
                    'seminar' => $seminar,
                ];

            }
        }

        return $out;
    }
}
