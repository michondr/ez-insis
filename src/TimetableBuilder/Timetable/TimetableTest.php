<?php

namespace App\TimetableBuilder\Timetable;

use App\DateTime\Time;
use App\TimetableBuilder\Timetable\Action\Entry\Entry;
use App\TimetableBuilder\Timetable\Action\Room\Room;
use App\TimetableBuilder\Timetable\Action\Subject\Subject;
use App\TimetableBuilder\Timetable\Action\Teacher\Teacher;
use App\TimetableBuilder\Timetable\Action\TimetableAction;
use PHPUnit\Framework\TestCase;

class TimetableTest extends TestCase
{
    public function testHasSpaceFor()
    {
        $comparisonsArray = [
            //starts before interval
            ['from' => '10:45', 'to' => '16:05', 'expects' => true], // BEFORE, before start, before end
            ['from' => '10:45', 'to' => '16:15', 'expects' => false], // BEFORE, at start
            ['from' => '10:45', 'to' => '17:00', 'expects' => false], // BEFORE, after start, before end
            ['from' => '10:45', 'to' => '17:45', 'expects' => false], // BEFORE, at end
            ['from' => '10:45', 'to' => '17:50', 'expects' => false], // BEFORE, after start, after end
            //starts at start
            ['from' => '16:15', 'to' => '17:40', 'expects' => false], // AT START, before end
            ['from' => '16:15', 'to' => '17:45', 'expects' => false], // AT START, at end
            ['from' => '16:15', 'to' => '17:50', 'expects' => false], // AT START, after end
            //starts in interval
            ['from' => '17:00', 'to' => '17:40', 'expects' => false], // IN, before end
            ['from' => '17:00', 'to' => '17:45', 'expects' => false], // IN, at end
            ['from' => '17:00', 'to' => '17:50', 'expects' => false], // IN, after end
            //starts at end
            ['from' => '17:45', 'to' => '17:50', 'expects' => false], // AT END, end after
            //starts after interval
            ['from' => '18:00', 'to' => '19:30', 'expects' => true], // BEFORE, after end
        ];

        foreach ($comparisonsArray as $item) {
            // starts same, ends same
            $action = new TimetableAction(
                1,
                'mon',
                Time::fromHI($item['from']),
                Time::fromHI($item['to']),
                new Entry('lecture'),
                new Subject(152089, '4ME213 3D Graphics', ['Day off: 28/10/2019']),
                new Room(1298, 'RB 336', 'WCH'),
                new Teacher(54275, 'J. Bubeníček')
            );

            self::assertSame($item['expects'], $this->getTimetable()->hasSpaceFor($action));
        }
    }

    /** @dataProvider getScoreDataProvider */
    public function testGetScore(array $actions, float $expectedResult)
    {
        $timetable = new Timetable();

        foreach ($actions as $action) {
            $timetable->addAction($action);
        }

        self::assertSame($expectedResult, $timetable->getScore(count($actions)));
    }

    public function getScoreDataProvider()
    {
        return [
            [
                'actions' => [
                    $this->getAction('mon', new Time(16, 15), new Time(17, 45)),
                ],
                'expectedScore' => 93.33,
            ],
            [
                'actions' => [
                    $this->getAction('mon', new Time(12, 45), new Time(16, 00)),
                    $this->getAction('mon', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('mon', new Time(18, 00), new Time(19, 30)),
                ],
                'expectedScore' => 90.86,
            ],
            [
                'actions' => [
                    $this->getAction('mon', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('tue', new Time(16, 15), new Time(17, 45)),
                ],
                'expectedScore' => 86.67,
            ],
            [
                'actions' => [
                    $this->getAction('mon', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('tue', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('tue', new Time(18, 00), new Time(19, 30)),
                ],
                'expectedScore' => 85.38,
            ],
            [
                'actions' => [
                    $this->getAction('mon', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('tue', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('wed', new Time(18, 00), new Time(19, 30)),
                ],
                'expectedScore' => 80.0,
            ],
            [
                'actions' => [
                    $this->getAction('mon', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('tue', new Time(16, 15), new Time(17, 45)),
                    $this->getAction('wed', new Time(18, 00), new Time(19, 30)),
                    $this->getAction('wed', new Time(9, 30), new Time(9, 00)),
                    $this->getAction('thu', new Time(18, 00), new Time(19, 30)),
                    $this->getAction('thu', new Time(9, 30), new Time(9, 00)),
                ],
                'expectedScore' => 58.33,
            ],
        ];
    }

    //todo remove function
    private function getTimetable(): Timetable
    {
        $timetable = new Timetable();
        $action = $this->getAction('mon', new Time(16, 15), new Time(17, 45));

        $timetable->addAction($action);

        return $timetable;
    }

    private function getAction(string $day, Time $from, Time $to): TimetableAction
    {
        return new TimetableAction(
            1,
            $day,
            $from,
            $to,
            new Entry('lecture'),
            new Subject(152089, '4ME213 3D Graphics', ['Day off: 28/10/2019']),
            new Room(1298, 'RB 336', 'WCH'),
            new Teacher(54275, 'J. Bubeníček')
        );
    }
}
