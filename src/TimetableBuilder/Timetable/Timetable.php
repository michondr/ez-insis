<?php

namespace App\TimetableBuilder\Timetable;

use App\DateTime\Day;
use App\DateTime\Time;
use App\TimetableBuilder\Builder\Exception\TimestampNotEmptyException;
use App\TimetableBuilder\Timetable\Action\TimetableAction;
use App\TimetableBuilder\Timetable\Score\ScoreByFilledAmount;
use App\TimetableBuilder\Timetable\Score\ScoreByFreeDays;
use App\TimetableBuilder\Timetable\Score\ScoreByLearnedTime;
use Exception;
use JsonSerializable;

class Timetable implements JsonSerializable
{
    private $days = [];
    private $score = [];

    public function __construct()
    {
        foreach (Day::getDayNamesShort() as $dayNum => $dayName) {
            $this->days[$dayName] = [];
        }
    }

    public function jsonSerialize()
    {
        return [
            'score' => $this->score,
            'hash' => md5(json_encode($this->days)),
            'data' => $this->days,
        ];
    }

    public function getDays(): array
    {
        return $this->days;
    }

    public function addAction(?TimetableAction $action)
    {
        if (is_null($action)) {
            return;
        }

        if (!$this->hasSpaceFor($action)) {
            throw new TimestampNotEmptyException();
        }

        $this->days[$action->getDayOfWeek()][$action->getTimestamp()] = $action;
    }

    public function hasSpaceFor(TimetableAction $action)
    {
        $dayTimes = array_keys($this->days[$action->getDayOfWeek()]);

        if (empty($dayTimes)) {
            return true;
        }

        foreach ($dayTimes as $dayTime) {
            $exploded = explode('-', $dayTime);
            $from = Time::fromHI($exploded[0])->toTimestamp();
            $to = Time::fromHI($exploded[1])->toTimestamp();

            $actionFrom = $action->getTimeFrom()->toTimestamp();
            $actionTo = $action->getTimeTo()->toTimestamp();

            $actionIsBefore = ($actionTo < $from);
            $actionIsAfter = ($actionFrom > $to);

            if (!$actionIsBefore and !$actionIsAfter) {
                return false;
            }
        }

        return true;
    }

    public function getScore(int $inputOptions): float
    {
        $subjectCount = $this->getActionsCount();
        if ($subjectCount === 0) {
            throw  new Exception('empty timetable');
        }

        $this->score = [
            'byFilledAmount' => round(ScoreByFilledAmount::count($this, $inputOptions), 3),
            'byFreeDays' => round(ScoreByFreeDays::count($this, $inputOptions), 3),
            'byLearnedTime' => round(ScoreByLearnedTime::count($this, $inputOptions), 3),
        ];

        $sum = round(array_sum($this->score) / 3 * 100, 2);
        $this->score['total'] = $sum.'%';

        return $sum;
    }

    public function getActionsCount(): int
    {
        $actionFilled = 0;

        foreach ($this->days as $day) {
            $actionFilled += count($day);
        }

        return $actionFilled;
    }

    public function orderTimes()
    {
        foreach ($this->days as $dayName => &$day) {
            if (count($day) <= 1) {
                continue;
            }

            uksort(
                $day,
                function ($a, $b) {
                    $aFrom = Time::fromHI(explode('-', $a)[0])->toTimestamp();
                    $bFrom = Time::fromHI(explode('-', $b)[0])->toTimestamp();

                    return $aFrom > $bFrom;
                }
            );
        }
    }
}
