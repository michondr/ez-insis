<?php

namespace App\TimetableBuilder\Timetable\SubjectActions;

use App\TimetableBuilder\Timetable\Action\TimetableAction;
use App\TimetableBuilder\Timetable\SubjectActions\Exception\UnableToAddActionToSubjectActionsException;

class SubjectActions
{
    private $subjectIdForSearch;
    private $lectures;
    private $seminars;

    public function __construct(int $subjectIdForSearch)
    {
        $this->subjectIdForSearch = $subjectIdForSearch;

        $this->lectures = [];
        $this->seminars = [];
    }

    public function getSubjectIdForSearch(): int
    {
        return $this->subjectIdForSearch;
    }

    public function getLectures(): array
    {
        return $this->lectures;
    }

    public function getSeminars(): array
    {
        return $this->seminars;
    }

    public function addLecture(TimetableAction $lecture)
    {
        if (!$lecture->getEntry()->isLecture()) {
            throw new UnableToAddActionToSubjectActionsException('this is not a lecture');
        }

        $this->lectures[] = $lecture;
    }

    public function addSeminars(TimetableAction $seminar)
    {
        if (!$seminar->getEntry()->isSeminar()) {
            throw new UnableToAddActionToSubjectActionsException('this is not a seminar');
        }

        $this->seminars[] = $seminar;
    }
}
