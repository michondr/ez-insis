<?php

namespace App\TimetableBuilder\Timetable\SubjectActions\Exception;

use App\TimetableBuilder\Timetable\Exception\TimetableException;

class UnableToAddActionToSubjectActionsException extends TimetableException
{
}
