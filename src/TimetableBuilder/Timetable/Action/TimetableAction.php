<?php

namespace App\TimetableBuilder\Timetable\Action;

use App\DateTime\Time;
use App\TimetableBuilder\Timetable\Action\Entry\Entry;
use App\TimetableBuilder\Timetable\Action\Room\Room;
use App\TimetableBuilder\Timetable\Action\Subject\Subject;
use App\TimetableBuilder\Timetable\Action\Teacher\Teacher;
use JsonSerializable;

class TimetableAction implements JsonSerializable
{
    private $setId;
    private $dayOfWeek;
    private $timeFrom;
    private $timeTo;
    private $entry;
    private $subject;
    private $room;
    private $teacher;

    public function __construct(
        int $setId,
        string $dayOfWeek,
        Time $timeFrom,
        Time $timeTo,
        Entry $entry,
        Subject $subject,
        Room $room,
        Teacher $teacher
    ) {
        $this->setId = $setId;
        $this->dayOfWeek = $dayOfWeek;
        $this->timeFrom = $timeFrom;
        $this->timeTo = $timeTo;
        $this->entry = $entry;
        $this->subject = $subject;
        $this->room = $room;
        $this->teacher = $teacher;
    }

    public function getDayOfWeek(): string
    {
        return $this->dayOfWeek;
    }

    public function getTimeFrom(): Time
    {
        return $this->timeFrom;
    }

    public function getTimeTo(): Time
    {
        return $this->timeTo;
    }

    public function getEntry(): Entry
    {
        return $this->entry;
    }

    public function getSubject(): Subject
    {
        return $this->subject;
    }

    public function getRoom(): Room
    {
        return $this->room;
    }

    public function getTeacher(): Teacher
    {
        return $this->teacher;
    }

    public function jsonSerialize()
    {
        if (isEnvDev()) {
            return strtr('entry - (roomName) subjName - teacher',
                [
                    'entry' => $this->entry->isLecture() ? 'LEC' : 'SEM',
                    'roomName' => $this->room->getName(),
                    'subjName' => $this->subject->getName(),
                    'teacher' => $this->teacher->getName(),
                ]
            );
        }

        return [
            "day" => $this->getDayOfWeek(),
            "time_from" => $this->getTimeFrom()->toHI(),
            "time_until" => $this->getTimeTo()->toHI(),
            "subject" => [
                "name" => $this->getSubject()->getName(),
                "id" => $this->getSubject()->getId(),
            ],
            "entry" => $this->getEntry()->getType(),
        ];
    }

    public function getTimestamp(): string
    {
        return $this->getTimeFrom()->toHI().'-'.$this->getTimeTo()->toHI();
    }
}
