<?php

namespace App\TimetableBuilder\Timetable\Action\Subject;

class Subject
{
    private $id;
    private $name;
    private $notes;

    public function __construct(
        int $id,
        string $name,
        array $notes
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->notes = $notes;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNotes(): array
    {
        return $this->notes;
    }
}
