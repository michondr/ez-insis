<?php

namespace App\TimetableBuilder\Timetable\Action\Subject;

class SubjectParser
{
    public static function parse(array $data): Subject
    {
        $id = $data['id'];
        $name = $data['name'];
        $notes = $data['notes'];

        return new Subject($id, $name, $notes);
    }
}
