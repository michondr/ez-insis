<?php

namespace App\TimetableBuilder\Timetable\Action\Teacher;

class TeacherParser
{
    public static function parse(array $data): Teacher
    {
        $id = $data['id'];
        $name = $data['name'];

        return new Teacher($id, $name);
    }
}
