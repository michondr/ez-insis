<?php

namespace App\TimetableBuilder\Timetable\Action\Entry\Exception;

use App\TimetableBuilder\Timetable\Exception\TimetableException;

class UnknownEntryTypeException extends TimetableException
{
    public function __construct(string $entry)
    {
        parent::__construct(
            strtr('unknown entry "{ENTRY}"', ['{ENTRY}' => $entry]),
            $this->getCode(),
            $this->getPrevious()
        );
    }
}
