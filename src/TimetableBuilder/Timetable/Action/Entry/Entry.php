<?php

namespace App\TimetableBuilder\Timetable\Action\Entry;

class Entry
{
    const ALLOWED_TYPES = ['seminar', 'lecture'];

    private $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isSeminar(): bool
    {
        return $this->type === 'seminar';
    }

    public function isLecture(): bool
    {
        return $this->type === 'lecture';
    }
}
