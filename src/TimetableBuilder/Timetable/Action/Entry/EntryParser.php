<?php

namespace App\TimetableBuilder\Timetable\Action\Entry;

use App\TimetableBuilder\Timetable\Action\Entry\Exception\UnknownEntryTypeException;

class EntryParser
{
    public static function parse(string $data): Entry
    {
        if (!in_array($data, Entry::ALLOWED_TYPES)) {
            throw new UnknownEntryTypeException($data);
        }

        return new Entry($data);
    }
}
