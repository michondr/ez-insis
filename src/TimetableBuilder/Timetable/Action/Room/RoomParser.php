<?php

namespace App\TimetableBuilder\Timetable\Action\Room;

class RoomParser
{
    public static function parse(array $data): Room
    {
        $id = $data['id'];
        $name = $data['name'];
        $location = RoomLocationTypes::getLocationForRoomName($name);

        return new Room($id, $name, $location);
    }
}
