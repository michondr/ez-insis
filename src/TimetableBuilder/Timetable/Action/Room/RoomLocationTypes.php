<?php

namespace App\TimetableBuilder\Timetable\Action\Room;

use App\TimetableBuilder\Timetable\Action\Room\Exception\MissingDistanceTableElementException;
use App\TimetableBuilder\Timetable\Action\Room\Exception\UnableToParseBuildingNameException;

class RoomLocationTypes
{
    const ALLOWED_TYPES = ['JM', 'WCH'];
    const BUILDING_TO_TYPE = [
        'JM' => 'JM',
        'RB' => 'WCH',
        'SB' => 'WCH',
        'IB' => 'WCH',
        'NB' => 'WCH',
        'XPT' => 'JAR',
        'Vencovského' => 'WCH',
        'Likešova' => 'WCH',
        '??' => '',
        '???' => '',
        '????' => '',
        '?????' => '',
        'Místnost' => '',
        'ONLINE' => '',
    ];

    public static function getTimeBetweenPlaces(string $from, string $to)
    {
        $fromTable = self::getDistanceTable()[$from] ?? null;
        $fromTableInverted = self::getDistanceTable()[$to] ?? null;

        // on first index one of approaches must return nonempty value
        if (is_null($fromTable) and is_null($fromTableInverted)) {
            throw new MissingDistanceTableElementException($from, $to);
        }

        $toValue = $fromTable[$to] ?? null;
        $toValueInverted = $fromTableInverted[$to] ?? null;

        // on second index one of approaches must return nonempty value
        if (is_null($toValue) and is_null($toValueInverted)) {
            throw new MissingDistanceTableElementException($from, $to);
        }

        if (is_null($toValue)) {
            return $toValueInverted;
        }

        return $toValue;
    }

    public static function getLocationForRoomName(string $roomName): string
    {
        $building = explode(' ', $roomName)[0];

        if (!is_string($building)) {
            throw new UnableToParseBuildingNameException();
        }

        return self::BUILDING_TO_TYPE[$building];
    }

    private static function getDistanceTable(): array
    {
        return [
            'JM' => [
                'WCH' => '40m',
            ],
        ];
    }
}
