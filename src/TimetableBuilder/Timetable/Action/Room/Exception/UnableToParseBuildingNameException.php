<?php

namespace App\TimetableBuilder\Timetable\Action\Room\Exception;

use App\TimetableBuilder\Timetable\Exception\TimetableException;

class UnableToParseBuildingNameException extends TimetableException
{
}
