<?php

namespace App\TimetableBuilder\Timetable\Action\Room\Exception;

use App\TimetableBuilder\Timetable\Exception\TimetableException;

class MissingDistanceTableElementException extends TimetableException
{
    public function __construct(string $from, string $to)
    {
        parent::__construct(
            strtr('missing info from "{FROM}" to "{TO}"', ['{FROM}' => $from, '{TO}' => $to]),
            $this->getCode(),
            $this->getPrevious()
        );
    }
}
