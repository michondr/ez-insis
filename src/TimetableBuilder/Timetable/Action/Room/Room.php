<?php

namespace App\TimetableBuilder\Timetable\Action\Room;

class Room
{
    private $id;
    private $name; //ie RB 336
    private $location; // JM or WCH or ....

    public function __construct(
        int $id,
        string $name,
        string $location
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->location = $location;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocation()
    {
        return $this->location;
    }
}
