<?php

namespace App\TimetableBuilder\Timetable\Action;

use App\DateTime\Time;
use App\Logging\ExceptionLogger;
use App\TimetableBuilder\Timetable\Action\Entry\EntryParser;
use App\TimetableBuilder\Timetable\Action\Entry\Exception\UnknownEntryTypeException;
use App\TimetableBuilder\Timetable\Action\Room\RoomParser;
use App\TimetableBuilder\Timetable\Action\Subject\SubjectParser;
use App\TimetableBuilder\Timetable\Action\Teacher\TeacherParser;

class TimetableActionParser
{
    public static function parse(int $setId, array $data)
    {
        $dayOfWeek = $data['day'];
        $timeFrom = Time::fromHI($data['time_from']);
        $timeTo = Time::fromHI($data['time_until']);
        $entry = EntryParser::parse($data['entry']);
        $subject = SubjectParser::parse($data['subject']);
        $room = RoomParser::parse($data['room']);
        $teacher = TeacherParser::parse($data['teacher']);

        return new TimetableAction(
            $setId,
            $dayOfWeek,
            $timeFrom,
            $timeTo,
            $entry,
            $subject,
            $room,
            $teacher
        );
    }

    public static function parseAll(int $setId, array $data)
    {
        $actions = [];

        foreach ($data as $item) {
            try {
                $actions[] = self::parse($setId, $item);
            } catch (UnknownEntryTypeException $e) {
//                $this->exceptionLogger->logException($e);
                continue;
            }
        }

        return $actions;
    }
}
