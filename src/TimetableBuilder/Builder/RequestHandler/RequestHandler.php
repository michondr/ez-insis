<?php

namespace App\TimetableBuilder\Builder\RequestHandler;

use App\DataCrawler\ScrapeLanguageResolver;
use App\DataCrawler\TimeTableItemCrawler;
use App\TimetableBuilder\Builder\Requirement\RequirementCollector;
use App\TimetableBuilder\Builder\Requirement\RequirementsFilter;
use App\TimetableBuilder\Builder\Types\FactorialBuilder\FactorialBuilder;
use App\TimetableBuilder\Timetable\Action\TimetableActionParser;
use App\TimetableBuilder\Timetable\ActionOption\ActionOptionGenerator;
use Symfony\Component\HttpFoundation\Request;

class RequestHandler
{
    private $itemCrawler;
    private $requirementsFilter;
    private $requirementCollector;
    private $actionOptionGenerator;
    private $builder;

    public function __construct(
        TimeTableItemCrawler $itemCrawler,
        RequirementsFilter $requirementsFilter,
        RequirementCollector $requirementCollector,
        ActionOptionGenerator $actionOptionGenerator,
        FactorialBuilder $builder
    ) {
        $this->itemCrawler = $itemCrawler;
        $this->requirementsFilter = $requirementsFilter;
        $this->requirementCollector = $requirementCollector;
        $this->actionOptionGenerator = $actionOptionGenerator;
        $this->builder = $builder;
    }

    public function handleSetRequest(Request $request): array
    {
        $resolvedLanguage = ScrapeLanguageResolver::resolveLanguage($request);

        $requirements = $this->requirementCollector->getRequirementsFromRequest($request);
        $actions = [];

        foreach ($requirements as $setId => $itsRequirements) {
            $crawledArray = $this->itemCrawler->crawlItemOrReturnCached($setId, $resolvedLanguage);
            $parsedActions = TimetableActionParser::parseAll($setId, $crawledArray['data']);

            $actions[$setId] = $this->requirementsFilter->filter($parsedActions, $itsRequirements);
        }

        $sortedActionsToOptions = $this->actionOptionGenerator->sort($actions);
        $build = $this->builder->build($sortedActionsToOptions);

        //pass values to counting worker
        //save to cache
        //make available to fetch on request
        if (count($actions) > 50) {
            $smallActions = 'more than 50';
        }

        if (count($build) > 50) {
            $smallBuild = array_slice($build, 0, 50);
        }

        return [
            'optionsCountPossible' => FactorialBuilder::countOptions($sortedActionsToOptions),
            'optionsCountBuilt' => count($build),
            'buildHash' => '',
            'estimatedTime' => 'don\'t ask me',
            'price' => 'don\'t ask me',
            'actions' => $smallActions ?? $actions,
            'build' => $smallBuild ?? $build,
        ];
    }

    public function handleGetRequest(Request $request, int $buildHash): array
    {
        return [isset($request), isset($buildHash)];
    }
}
