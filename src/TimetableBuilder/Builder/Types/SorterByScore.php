<?php

namespace App\TimetableBuilder\Builder\Types;

use App\TimetableBuilder\Timetable\Timetable;

class SorterByScore
{
    public function sort(array &$timetables, int $inputOptions)
    {
        usort(
            $timetables,
            function (Timetable $a, Timetable $b) use ($inputOptions) {
                return $a->getScore($inputOptions) < $b->getScore($inputOptions);
            }
        );
    }
}
