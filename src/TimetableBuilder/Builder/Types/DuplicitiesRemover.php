<?php

namespace App\TimetableBuilder\Builder\Types;

use App\TimetableBuilder\Timetable\Timetable;

class DuplicitiesRemover
{
    public function removeDuplicities(array &$timetables)
    {
        $out = [];

        /** @var Timetable $timetable */
        foreach ($timetables as $timetable) {
            $timetable->orderTimes();
            $out[md5(json_encode($timetable))] = $timetable;
        }

        $timetables = $out;
    }
}
