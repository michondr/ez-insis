<?php

namespace App\TimetableBuilder\Builder\Types\FactorialBuilder;

use App\TimetableBuilder\Builder\Exception\TimestampNotEmptyException;
use App\TimetableBuilder\Builder\Types\DuplicitiesRemover;
use App\TimetableBuilder\Builder\Types\SorterByScore;
use App\TimetableBuilder\Timetable\ActionOption\ActionOption;
use App\TimetableBuilder\Timetable\Timetable;

class FactorialBuilder
{
    private $duplicitiesRemover;
    private $sorterByScore;

    public function __construct(
        DuplicitiesRemover $duplicitiesRemover,
        SorterByScore $sorterByScore
    ) {
        $this->duplicitiesRemover = $duplicitiesRemover;
        $this->sorterByScore = $sorterByScore;
    }

    public static function countOptions(array $sortedData): int
    {
        $optionVersions = [];

        /** @var ActionOption $option */
        foreach ($sortedData as $key => $option) {
            $optionVersions[] = count($option->getAllPossibleVersions());
        }

        $countOfOptions = count($sortedData);
        $multiplicationOfVersions = array_product($optionVersions);

        return (gmp_strval(gmp_fact($countOfOptions)) * $multiplicationOfVersions);
    }

    public function build(array $sortedData): array
    {
        $options = [];
        $entriesCounts = [];

        /** @var ActionOption $option */
        foreach ($sortedData as $key => $option) {
            $entriesCounts[] = $option->getEntriesCount();

            foreach ($option->getAllPossibleVersions() as $version) {
                $options[$key][] = $version;
            }
        }

//        vdx($options);
        $timetables = $this->loop([], [], $options, []);

        $this->duplicitiesRemover->removeDuplicities($timetables);
        $this->sorterByScore->sort($timetables, array_sum($entriesCounts));

        return $timetables;
    }

    private function loop(array $timetables, array $usedKeys, array $options, array $optionsToAdd, int $level = 0)
    {
        $myTimetables = [];

        //take each subject as BASE
        foreach ($options as $key1 => $subjectOptions) {
            if (in_array($key1, $usedKeys)) {
                continue;
            } else {
                $usedKeys[] = $key1;
            }

            //take each option of BASE subject
            foreach ($subjectOptions as $option) {
                $optionsToAdd[] = $option;

                if (count($usedKeys) === count($options)) {
                    try {
                        $myTimetables[] = $this->getTimetable($optionsToAdd);
                    } /** @noinspection PhpRedundantCatchClauseInspection */
                    catch (TimestampNotEmptyException $e) {
                        continue;
                    }
                } else {
                    $timetables = $this->loop($timetables, $usedKeys, $options, $optionsToAdd, ($level + 1));
                }
                array_pop($optionsToAdd);
            }
            array_pop($usedKeys);
        }

        $timetables = array_merge($timetables, $myTimetables);

        return $timetables;
    }

    private function getTimetable(array $options): Timetable
    {
        $timetable = new Timetable();

        foreach ($options as $option) {
            // add BASE lecture and seminar, then TOP1, then TOP2
            $timetable->addAction($option['lecture']);
            $timetable->addAction($option['seminar']);
        }

        return $timetable;
    }
}
