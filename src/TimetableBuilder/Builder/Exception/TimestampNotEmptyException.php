<?php

namespace App\TimetableBuilder\Builder\Exception;

class TimestampNotEmptyException extends \Exception
{
}
