<?php

namespace App\TimetableBuilder\Builder\Requirement;

use App\TimetableBuilder\Timetable\Action\TimetableAction;

interface IRequirement
{
    public function getName(): string;

    public function shouldAdd(TimetableAction $action): bool;
}
