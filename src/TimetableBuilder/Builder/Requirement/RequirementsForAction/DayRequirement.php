<?php

namespace App\TimetableBuilder\Builder\Requirement\RequirementsForAction;

use App\TimetableBuilder\Builder\Requirement\AbstractRequirement;
use App\TimetableBuilder\Builder\Requirement\IRequirement;
use App\TimetableBuilder\Timetable\Action\TimetableAction;

class DayRequirement extends AbstractRequirement implements IRequirement
{
    public function getName(): string
    {
        return 'day';
    }

    public function shouldAdd(TimetableAction $action): bool
    {
        $comparableValues = $this->getComparableValues();

        $actionValue = $action->getDayOfWeek();
        $addBecauseItIsAllowed = in_array($actionValue, $comparableValues);

        return $addBecauseItIsAllowed;
    }
}
