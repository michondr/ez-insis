<?php

namespace App\TimetableBuilder\Builder\Requirement;

abstract class AbstractRequirement
{
    private $comparableValues;

    public function getComparableValues(): array
    {
        return $this->comparableValues;
    }

    public function setComparableValues(array $comparableValues): void
    {
        $this->comparableValues = $comparableValues;
    }
}
