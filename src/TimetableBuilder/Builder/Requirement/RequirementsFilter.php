<?php

namespace App\TimetableBuilder\Builder\Requirement;

use App\TimetableBuilder\Timetable\Action\TimetableAction;

class RequirementsFilter
{
    public function filter(array $data, array $requirements)
    {
        $fulfilledActions = [];

        /** @var TimetableAction $action */
        foreach ($data as $action) {
            $fulfilledCount = 0;

            /** @var IRequirement $requirement */
            foreach ($requirements as $requirement) {
                if ($requirement->shouldAdd($action)) {
                    $fulfilledCount += 1;
                }
            }

            if ($fulfilledCount === count($requirements)) {
                $fulfilledActions[] = $action;
            }
        }

        usort(
            $fulfilledActions,
            function (TimetableAction $a, TimetableAction $b) {
                return json_encode($a) > json_encode($b);
            }
        );

        return $fulfilledActions;
    }
}
