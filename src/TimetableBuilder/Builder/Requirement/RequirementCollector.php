<?php

namespace App\TimetableBuilder\Builder\Requirement;

use App\TimetableBuilder\Builder\Requirement\Exception\InvalidInputException;
use App\TimetableBuilder\Builder\Requirement\Exception\MissingRequirementsException;
use App\TimetableBuilder\Builder\Requirement\Exception\UnknownRequirementException;
use App\TimetableBuilder\Builder\Requirement\RequirementsForAction\DayRequirement;
use App\TimetableBuilder\Builder\Requirement\RequirementsForAction\SubjectIdRequirement;
use Symfony\Component\HttpFoundation\Request;

class RequirementCollector
{
    private $availableRequirements;

    public function __construct()
    {
        $this->availableRequirements[] = new SubjectIdRequirement();
        $this->availableRequirements[] = new DayRequirement();
    }

    public function getRequirementByName(string $name): AbstractRequirement
    {
        /** @var IRequirement $requirement */
        foreach ($this->availableRequirements as $requirement) {
            if ($requirement->getName() === $name) {
                $class = get_class($requirement);

                return new $class;
            }
        }

        throw new UnknownRequirementException($name);
    }

    public function getRequirementsFromRequest(Request $request): array
    {
        $content = json_decode($request->getContent(), true);

        if ($content === false) {
            throw new InvalidInputException();
        }

        $requirements = [];

        foreach ($content as $element) {
            if (!isset($requirements[$element['set']])) {
                $requirements[$element['set']] = [];
            }

            if(!isset($element['requirements']) or empty($element['requirements'])){
                throw new MissingRequirementsException($element['set']);
            }
            foreach ($element['requirements'] as $name => $value) {
                $requirement = $this->getRequirementByName($name);
                $requirement->setComparableValues($value);
                $requirements[$element['set']][] = $requirement;
            }

        }

        return $requirements;
    }
}
