<?php

namespace App\TimetableBuilder\Builder\Requirement\Exception;

use App\Response\Json\Exception\AllowedException;

class MissingRequirementsException extends AllowedException
{
    public function __construct(int $setId)
    {
        parent::__construct('set with id '.$setId.' must have its requirements');
    }
}
