<?php

namespace App\TimetableBuilder\Builder\Requirement\Exception;

use App\TimetableBuilder\Timetable\Exception\TimetableException;

class UnknownRequirementException extends TimetableException
{
    public function __construct(string $name)
    {
        parent::__construct(
            strtr('unknown requirement "{NAME}"', ['{NAME}' => $name]),
            $this->getCode(),
            $this->getPrevious()
        );
    }
}
