<?php

namespace App\TimetableBuilder\Builder\Requirement\Exception;

use App\Response\Json\Exception\AllowedException;

class InvalidInputException extends AllowedException
{

}