<?php

namespace App\AccessControl;

use App\AccessControl\AccessToken\AccessTokenStorage;
use App\Cache\CacheRouteLimit;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\User;

class RequestLimiter
{
    private $authorizationChecker;
    private $routeUserTtl;
    private $cache;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        CacheRouteLimit $cache
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->cache = $cache;
        $this->routeUserTtl = [
//            'data.time-table' => 60 * 60,
            'data.time-table' => 5,
            'data.some' => 26,
        ];
    }

    public function handleLimit(Request $request, ?User $user): ?Response
    {
        if (is_null($user) or $this->authorizationChecker->isGranted(AccessTokenStorage::ROLE_UNLIMITED)) {
            return null;
        }

        $route = $request->get('_route');
        $userName = $user->getUsername();

        $ttl = $this->getTtl($route);
        $key = $this->getCacheKey($route, $userName);

        if (is_null($ttl)) {
            return null;
        }

        if ($this->cache->has($key)) {
            return new JsonResponse(['message' => 'user "'.$userName.'" can call this route once every '.$ttl.' seconds'], Response::HTTP_TOO_MANY_REQUESTS);
        } else {
            $this->cache->set($key, null, $ttl);
        }

        return null;
    }

    private function getTtl(string $route)
    {
        foreach ($this->routeUserTtl as $routeNeedle => $routeTtl) {
            if (strpos($route, $routeNeedle) === 0) {
                return $routeTtl;
            }
        }

        return null;
    }

    private function getCacheKey(string $route, string $userName)
    {
        return $route.'_'.$userName;
    }
}
