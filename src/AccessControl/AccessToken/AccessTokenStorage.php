<?php

namespace App\AccessControl\AccessToken;

class AccessTokenStorage
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_API = 'ROLE_API';
    const ROLE_UNLIMITED = 'ROLE_UNLIMITED';

    public static function getUsers()
    {
        return [
            'michondr' => [
                'token' => '593c844948cc32d521bf3d02123c7583',
                'email' => 'me@michondr.cz',
                'roles' => [self::ROLE_API, self::ROLE_ADMIN, self::ROLE_UNLIMITED],
            ],
            'time-table-tweak' => [
                'token' => '1ec797627eca0f34fd2377c983ce37b9',
                'email' => 'me@michondr.cz',
                'roles' => [self::ROLE_API],
            ],
            'studolog' => [
                'token' => '408fde379f977cf6da3e92026f1403e6',
                'email' => 'team@studolog.com',
                'roles' => [self::ROLE_API],
            ],
            'test-user' => [
                'token' => 'ffbe8ca9e8aeee36f3a0e6855ed33801',
                'email' => 'me@example.com',
                'roles' => [self::ROLE_API],
            ],
            'pilm03@vse.cz' => [
                'token' => '5d78a2a15a1c08235e934b774c51e761',
                'email' => 'pilm03@vse.cz',
                'roles' => [self::ROLE_API],
            ],
        ];
    }
}
