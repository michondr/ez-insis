<?php

namespace App\AccessControl;

use App\AccessControl\AccessToken\AccessTokenStorage;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    private $users;

    public function __construct()
    {
        $this->users = AccessTokenStorage::getUsers();
    }

    public function getUsernameForApiKey($apiKey)
    {
        foreach ($this->users as $username => $user) {
            if ($user['token'] === $apiKey) {
                return $username;
            }
        }

        return null;
    }

    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            $this->users[$username]['roles']
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // This is used for storing authentication in the session but in this example, the token is sent in each request, so authentication can be stateless.
        // Throwing this exception is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
