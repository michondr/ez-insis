<?php

function tstart(string $label)
{
    $GLOBALS['time_measurement'][$label] = microtime(true);
}

function tend(string $label)
{
    $end = microtime(true);
    $time = $end - $GLOBALS['time_measurement'][$label];
    $time *= 1000; // to seconds

    $watch = $label.': '.$time.' ms';
    dump($watch);

//    if ($GLOBALS['kernel']->getEnvironment() == "dev") {
//    } else {
//        var_dump($watch);
//    }
}

function isEnvDev()
{
    return $GLOBALS['env'] === 'dev';
}

function vd($data)
{
    if (is_object($data)) {
        dump($data);

        return;
    }

    if (is_array($data)) {
        echo json_encode($data);

        return;
    }

    echo $data;
}

function vdx($data)
{
    vd($data);
    die;
}
