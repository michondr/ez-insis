<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DevCommand extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('dev:command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $arr = [
            [
                'set' => 2715, //FIS WS 2019/2020
                'requirements' => [
                    'subject_id' => [
                        151844,  //4EK211 Basic Econometrics                1
                        151985,  //4IT101 Programming in Java Language      1
                    ],
                    'day' => [
                        'wed',
                    ],
                ],
            ],
            [
                'set' => 2695, //FMV WS 2019/2020
                'requirements' => [
                    'subject_id' => [
                        140516,  //2SE252 World Economy
                    ],
                    'day' => [
                        'tue',                                             // 1
                    ],
                ],
            ],
        ];

        dump(json_encode($arr));
    }

}