<?php

namespace App\Request;

use App\AccessControl\RequestLimiter;
use App\Logging\ExceptionLogger;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{

    private $container;
    private $annotationsReader;
    private $annotationClasses;
    private $requestLimiter;
    private $exceptionLogger;

    public function __construct(
        ContainerInterface $container,
        Reader $annotationsReader,
        RequestLimiter $requestLimiter,
        ExceptionLogger $exceptionLogger
    ) {
        $this->container = $container;
        $this->annotationsReader = $annotationsReader;
//        $this->annotationClasses[get_class($requestLimiter)] = $requestLimiter;
        $this->requestLimiter = $requestLimiter;
        $this->exceptionLogger = $exceptionLogger;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $annotations = $this->getAnnotation($request);

        $this->exceptionLogger->log('route called, ', ['route' => $request->get('_route')]);

        $token = $this->container->get('security.token_storage')->getToken();
        $user = is_null($token) ? null : $token->getUser();

        $response = $this->requestLimiter->handleLimit($request, $user);

        if ($response) {
            $event->setResponse($response);
            $event->stopPropagation();
        }
    }

    public function getAnnotation(Request $request)
    {
        $controllerActionIdentifier = $request->attributes->get('_controller');

        $controllerServiceName = substr($controllerActionIdentifier, 0, strpos($controllerActionIdentifier, '::'));
        $actionMethodName = substr($controllerActionIdentifier, strpos($controllerActionIdentifier, '::') + 2);

        $controllerService = $this->container->get($controllerServiceName);
        $controllerClassName = get_class($controllerService);

        $object = new \ReflectionClass($controllerClassName);
        $method = $object->getMethod($actionMethodName);

        return $this->annotationsReader->getMethodAnnotations($method);
    }

    private function getRequestLimiter(array $annotations)
    {
        foreach ($annotations as $annotation) {
            $class = get_class($annotation);
            if (array_key_exists($class, $this->annotationClasses)) {
                return $this->annotationClasses[$class];
            }
        }

        return null;
    }
}
