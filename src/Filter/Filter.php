<?php

namespace App\Filter;

use Symfony\Component\HttpFoundation\Request;

class Filter
{
    private $fields;
    private $originalArray;
    private $originalData;
    private $exactMatch;

    public function __construct(Request $request, array $originalArray, array $fieldNames)
    {
        foreach ($fieldNames as $name) {
            $this->fields[$name] = $request->get($name);
        }
        $this->originalArray = $originalArray;
        $this->originalData = $originalArray['data'];

        $this->exactMatch = (bool)$request->get('exact_match', true);
    }

    public function isEmpty()
    {
        return (count($this->getFilledFields()) === 0);
    }

    public function getFilledFields()
    {
        $notEmptyFields = [];

        foreach ($this->fields as $name => $value) {
            if (!is_null($value) and !empty($value)) {
                $notEmptyFields[$name] = $value;
            }
        }

        return $notEmptyFields;
    }

    public function getInOutputFormat()
    {
        //just for format of output;
        unset($this->originalArray['data']);

        $this->originalArray['filter'] = $this->getFilledFields();
        $this->originalArray['filter']['exact_match'] = $this->exactMatch;
        $this->originalArray['data'] = $this->applyToData();

        return $this->originalArray;
    }

    private function applyToData()
    {
        $filledFields = $this->getFilledFields();
        $itemsMatchingFilter = [];

        foreach ($this->originalData as $dataKey => $dataItem) {
            $matchedFields = 0;
            foreach ($dataItem as $key => $content) {
                if (is_array($content)) {
                    if (array_key_exists('id', $content) and in_array($key.'_id', array_keys($filledFields))) {
                        $key = $key.'_id';
                        $content = $content['id'];
                    } elseif (array_key_exists('name', $content) and in_array($key.'_name', array_keys($filledFields))) {
                        $key = $key.'_name';
                        $content = $content['name'];
                    }
                }

                if (!in_array($key, array_keys($filledFields))) {
                    continue;
                }

                if ($content === $filledFields[$key]) {
                    //matched exact?
                    ++$matchedFields;
                } elseif ((mb_strpos($content, $filledFields[$key]) !== false) and !$this->exactMatch) {
                    //matched LIKE if exact match is disabled?
                    ++$matchedFields;
                }
            }

            if (count($filledFields) === $matchedFields) {
                $itemsMatchingFilter[$dataKey] = $dataItem;
            }
        }

        return $itemsMatchingFilter;
    }
}
